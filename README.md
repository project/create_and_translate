# Create and Translate

> Creates a save-button on the node form that allows you to create a node and 
> go to the translate page.

## Installation

1. Install the module the [drupal way](https://www.drupal.org/documentation/install/modules-themes/modules-8)
2. No more settings needed.
