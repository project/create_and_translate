<?php

namespace Drupal\Tests\create_and_translate\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * This class provides methods specifically for testing something.
 *
 * @group create_and_translate
 */
class CreateAndTranslateFunctionalTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'create_and_translate',
    'language',
    'content_translation',
    'node',
    'taxonomy',
    'test_page_test',
  ];

  /**
   * A user with authenticated permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $user;

  /**
   * A user with admin permissions.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->config('system.site')->set('page.front', '/test-page')->save();
    $this->user = $this->drupalCreateUser([]);
    $this->adminUser = $this->drupalCreateUser([]);
    $this->adminUser->addRole($this->createAdminRole('admin', 'admin'));
    $this->adminUser->save();
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests if the module installation, won't break the site.
   */
  public function testInstallation() {
    $session = $this->assertSession();
    $this->drupalGet('<front>');
    $session->statusCodeEquals(200);
  }

  /**
   * Tests if uninstalling the module, won't break the site.
   */
  public function testUninstallation() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    $this->drupalGet('/admin/modules/uninstall');
    $session->statusCodeEquals(200);
    $page->checkField('edit-uninstall-create-and-translate');
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    // Confirm deinstall:
    $page->pressButton('edit-submit');
    $session->statusCodeEquals(200);
    $session->pageTextContains('The selected modules have been uninstalled.');
  }

  /**
   * Tests to see if the translation button works as intended.
   */
  public function testButtonWorksAndRedirects() {
    $session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Add a second language to the site:
    $this->drupalGet('/admin/config/regional/language/add');
    $session->statusCodeEquals(200);
    $page->selectFieldOption('edit-predefined-langcode', 'de');
    $page->pressButton('edit-predefined-submit');
    $session->statusCodeEquals(200);
    // Create translatable content manually, since for some reason we
    // can not use createContentType()
    $this->drupalGet('/admin/structure/types/add');
    $session->statusCodeEquals(200);
    $page->fillField('edit-name', 'Test');
    $page->fillField('edit-type', 'test');
    $page->fillField('edit-title-label', 'Title');
    $page->fillField('edit-language-configuration-content-translation', TRUE);
    $page->pressButton('edit-submit', TRUE);
    $session->statusCodeEquals(200);
    $session->pageTextContains('The content type Test has been added.');

    // Create a new test content and check if the button works:
    $this->drupalGet('/node/add/test');
    $session->statusCodeEquals(200);
    $session->elementExists('css', '#edit-create-and-translate');
    $page->fillField('edit-title-0-value', 'Test123');
    $page->pressButton('edit-create-and-translate');
    $session->statusCodeEquals(200);
    $session->addressEquals('/node/1/translations');
  }

}
