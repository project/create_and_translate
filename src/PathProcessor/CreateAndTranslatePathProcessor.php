<?php

namespace Drupal\create_and_translate\PathProcessor;

use Drupal\Core\PathProcessor\OutboundPathProcessorInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Symfony\Component\HttpFoundation\Request;

/**
 * A class to create and translate the path processor.
 */
class CreateAndTranslatePathProcessor implements OutboundPathProcessorInterface {

  /**
   * {@inheritdoc}
   */
  public function processOutbound($path, &$options = [], Request $request = NULL, BubbleableMetadata $bubbleable_metadata = NULL) {
    if (isset($options['query']['destination']) && stripos($options['query']['destination'], 'admin/content') > 0) {
      unset($options['query']['destination']);
    }
    return $path;
  }

}
